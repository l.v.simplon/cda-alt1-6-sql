
delimiter $$ ;
create function get_xp_to_next_level(next_level INT) returns INT
begin

declare a int;

-- https://bulbapedia.bulbagarden.net/wiki/Experience#Fluctuating
set a = (
  case
    when next_level <= 15 then (floor((next_level + 1 / 3)) + 24 )
    when next_level <= 36 then (       next_level           + 14 )
    else                       (      (next_level     / 2)  + 32 )
  end);


return power(next_level, 3) * (a / 50);


end$$

