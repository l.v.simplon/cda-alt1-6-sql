
delimiter $$ ;
create function get_xp_for_duration(start DATE, end DATE) returns INT
begin

  return abs(datediff(start, end)) * 22;

end$$

