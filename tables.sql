DROP DATABASE IF EXISTS woofing;
CREATE DATABASE IF NOT EXISTS woofing;
USE woofing;


CREATE TABLE users (
  id INT NOT NULL AUTO_INCREMENT,
    registration_date DATE NOT NULL,
    first_name  VARCHAR(30)     NOT NULL,
    last_name   VARCHAR(30)     NOT NULL,
    type VARCHAR(20) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE host_places (
  id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    notes VARCHAR(100) NOT NULL,
    capacity INT NOT NULL,
    nb VARCHAR(10) NOT NULL,
    street_name VARCHAR(100) NOT NULL,
    city_name VARCHAR(100) NOT NULL,
    country VARCHAR(50) NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCeS users (id) ON DELETE CASCADE
);

CREATE TABLE activities (
  id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE levels (
  user_id INT NOT NULL,
  activity_id INT NOT NULL,
  xp INT NOT NULL,
  level INT NOT NULL,

  PRIMARY KEY (user_id, activity_id),
  FOREIGN KEY (activity_id) REFERENCES activities (id),
  FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE activities_to_host_places (
  id INT NOT NULL AUTO_INCREMENT,
  host_place_id INT NOT NULL,
  activity_id INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (activity_id) REFERENCES activities (id),
  FOREIGN KEY (host_place_id) REFERENCES host_places (id)
);

CREATE TABLE activity_applications (
  id INT NOT NULL AUTO_INCREMENT,
  from_user_id INT NOT NULL,
  to_user_id INT NOT NULL,
  accepted VARCHAR(10) DEFAULT("pending"),
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,

  was_here BOOLEAN,

  PRIMARY KEY (id),
  FOREIGN KEY (from_user_id) REFERENCES users (id),
  FOREIGN KEY (to_user_id) REFERENCES users (id)
);

CREATE TABLE temporary_activities (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100),
  host_place_id INT NOT NULL,
  capacity INT NOT NULL,

  start_date DATE NOT NULL,
  end_date DATE NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (host_place_id) REFERENCES host_places (id)
);

CREATE TABLE temporary_activities_to_activities (
  id INT NOT NULL AUTO_INCREMENT,
  temporary_activities_id INT NOT NULL,
  activity_id INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (activity_id) REFERENCES activities (id),
  FOREIGN KEY (temporary_activities_id) REFERENCES temporary_activities (id)
);

CREATE TABLE temporary_activities_applications (
  -- id INT NOT NULL AUTO_INCREMENT,
  from_user_id INT NOT NULL,
  temporary_activity_id INT NOT NULL,
  accepted VARCHAR(10) DEFAULT("pending"),

  was_here BOOLEAN,

  -- PRIMARY KEY (id),
  PRIMARY KEY (from_user_id, temporary_activity_id),
  FOREIGN KEY (from_user_id) REFERENCES users (id),
  FOREIGN KEY (temporary_activity_id) REFERENCES temporary_activities (id)
);

CREATE TABLE notifications (
  id INT NOT NULL AUTO_INCREMENT,
  to_user_id INT NOT NULL,
  message varchar(1024) NOT NULL,
  date DATE NOT NULL,
  seen BOOLEAN DEFAULT(0),

  PRIMARY KEY (id),
  FOREIGN KEY (to_user_id) REFERENCES users (id)
);

