delimiter $$ ;
create trigger notification_tempactivity_user_accepted
  after update
  on temporary_activities_applications
  for each row
begin

DECLARE host_user_id INT;
DECLARE host_place_id INT;
DECLARE host_name varchar(100);
DECLARE activity_name varchar(100);

select ta.host_place_id
  into host_place_id
  from temporary_activities ta
  where ta.id = new.temporary_activity_id;

select user_id
  into host_user_id
  from host_places
  where id = host_place_id
;

-- select get_user_name(host_user_id) into host_name;
set host_name = get_user_name(host_user_id);

select name
  into activity_name
  from temporary_activities
  where id = new.temporary_activity_id
;

insert into notifications
(to_user_id, message, date)
values (
  new.from_user_id,
  concat(
    "User '", host_name, " ", new.accepted,
    " your application for the activity '", activity_name, "'."
  ),
  curdate()
);

end$$

