use woofing;

insert into users(id, registration_date, first_name, last_name, type)
values (100, now(), "Marie", "Pierre", "host");

insert into host_places(id, name, notes, capacity, nb, street_name, city_name, country, user_id)
values (100, "La boite à meuh", "Lorem ipsum", 3, "221B", "baker street", "London", "England", 100);

insert into activities_to_host_places(activity_id, host_place_id)
values (1, 1);

insert into activities_to_host_places(activity_id, host_place_id)
values (2, 1);

insert into activities_to_host_places(activity_id, host_place_id)
values (3, 1);

insert into activities_to_host_places(activity_id, host_place_id)
values (4, 1);

