use woofing;

select 
  ta.*,
  group_concat(a.name separator ', ') as activities
from temporary_activities_with_free_slots ta

left join temporary_activities_to_activities ta_to_a
  on ta_to_a.temporary_activities_id = ta.id

left join activities a
  on ta_to_a.activity_id = a.id

group by ta.id
  having ta.start_date > "2019-01-01"
     and slots_available > 0
;

insert into temporary_activities_applications values
(12, 1, "pending", NULL);


-- ...


update temporary_activities_applications
  set accepted = "accepted"
  where from_user_id = 12
    and temporary_activity_id = 1
;


-- ...

update temporary_activities_applications
  set was_here = 1
  where from_user_id = 12
    and temporary_activity_id = 1
;

