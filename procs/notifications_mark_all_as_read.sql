drop procedure if exists `notifications_mark_all_as_read`;

delimiter $$ ;
create procedure notifications_mark_all_as_read(in user_id int)
begin

update notifications
set seen=1
where to_user_id = user_id;

end$$

