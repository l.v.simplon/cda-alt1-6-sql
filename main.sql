source tables.sql;

source ./fixtures/main.sql

source ./procs/notifications_mark_all_as_read.sql
source ./procs/experience_add_to_user.sql

source ./views/temporary_activities_with_free_slots.sql

source ./triggers/notification_user_applied.sql
source ./triggers/notification_tempactivity_user_accepted.sql
source ./triggers/tempactiv_give_xp_when_present.sql

source ./funcs/get_user_name.sql
source ./funcs/get_xp_for_duration.sql
source ./funcs/get_xp_to_next_level.sql

